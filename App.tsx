/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Video from 'react-native-video';

const {height} = Dimensions.get('window');

const App = () => {
  return (
    <>
      <View style={styles.container}>
        <Video
          repeat={true}
          source={{
            uri:
              'https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4',
          }} // Can be a URL or a local file.
          resizeMode="cover"
          playInBackground={true}
          style={styles.backgroundVideo}
        />
        <View style={styles.body}>
          <View style={styles.menu}>
            <TouchableOpacity style={styles.menuItem}>
              <Text style={styles.menuItemText}>Learn More</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Text style={styles.menuItemText}>Read the docs</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: height,
  },
  menu: {
    width: 200,
  },
  menuItem: {
    backgroundColor: 'gray',
  },
  body: {
    flex: 1,
    paddingVertical: 32,
    paddingHorizontal: 24,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  menuItemText: {
    marginVertical: 8,
    marginHorizontal: 8,
    fontSize: 18,
    color: Colors.white,
  },
  backgroundVideo: {
    alignItems: 'stretch',
    backgroundColor: Colors.white,
    position: 'absolute',
    zIndex: -1,
    height: '100%',
    width: '100%',
  },
});

export default App;
